# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
def transmogrify(word, opt = {})
  defaults = {times: 2, upcase: true, reverse: false}

  options = defaults.merge(opt)
  p options
  transmogrified = word

  if options[:upcase] && options[:reverse]
    transmogrified = word.upcase.reverse
  elsif options[:reverse]
    transmogrified = word.reverse
  elsif options[:upcase]
    transmogrified = word.upcase
  end

  transmogrified * options[:times]

end
